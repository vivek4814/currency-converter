/**
* Copyright (c) 2021 HCL, Inc.. All rights reserved.
*/

import UIKit
import XCTest
@testable import CurrencyConverter

class ConverterViewModelTests: XCTestCase {
    
    var converterViewModel: ConverterViewModel?
     var convertModel : Converter?
    
    
    override func setUp() {
        super.setUp()
        converterViewModel = ConverterViewModel()
        guard let data = ConverterStub.converter(file: "sample_rates") else {
            return
        }
        do {
            guard let todo = try JSONSerialization.jsonObject(with: data, options: [])
                as? [String:Any] else {
                    debugPrint("error trying to convert data to JSON")
                    return
            }
            
            if let converterModel = Converter.objData(fromDict: todo as NSDictionary){
                convertModel = converterModel
            }
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
    }
    
 func testForInavlidUrl() -> Void {
        BaseServiceImpl.sharedInstance.requestApiWithUrlString(requestType: HtttpType.GET.rawValue, baseUrl: "testurl") { (dataObj, errorType, succes) in
           XCTAssertEqual(errorType, ErrorType.invalidUrl)
        }
    }
   
 
    
    func testForInvalidResponse() -> Void {
          BaseServiceImpl.sharedInstance.requestApiWithUrlString(requestType: HtttpType.GET.rawValue, baseUrl: "https://data.fixer.io/api/latest") { (dataObj, errorType, succes) in
              if succes {
                  if let response = dataObj {
                    let model = Converter.objData(fromDict: response as NSDictionary)
                      XCTAssertEqual(model?.success, false)
                  }
                  
              }
          }
      }
    
    func testForValidJSON() -> Void {
         guard let data = ConverterStub.converter(file: "sample_rates") else {
             return
         }
         let response = try? JSONSerialization.jsonObject(with: data, options: [])
             as? [String:Any]
         XCTAssertNotNil(Converter.objData(fromDict: response! as NSDictionary))
     }
    
    func testForValidResponse() -> Void {
        BaseServiceImpl.sharedInstance.requestApiWithUrlString(requestType: HtttpType.GET.rawValue, baseUrl: Constants.base_url) { (dataObj, errorType, succes) in
            if succes {
                if let response = dataObj {
                    XCTAssertNotNil(Converter.objData(fromDict: response as NSDictionary))
                }
                
            }
        }
    }
    
  

    func testFunctionForINRToEUR()  {
        let value = converterViewModel?.getConvertedEURValueFromINR(inrText:  convertModel?.rates?.iNR ?? 0.0, targetValue: convertModel?.rates?.iNR ??  0.0)
        XCTAssertEqual(value, 1)
    }
    
    func testFunctionForEURToINR()  {
        let value = converterViewModel?.getConvertedINRValueFromEUR(eurText: 1.00, targetValue: convertModel?.rates?.iNR ?? 0.0)
        XCTAssertEqual(value,convertModel?.rates?.iNR ?? 0.0)
    }
    
    func testFunctionForINRToEURForFail()  {
        let value = converterViewModel?.getConvertedEURValueFromINR(inrText:  convertModel?.rates?.iNR ?? 0.0, targetValue: convertModel?.rates?.iNR ?? 0.0)
        XCTAssertNotEqual(value, 2)
    }
    
    func testFunctionForEURToINRForFail()  {
        let value = converterViewModel?.getConvertedINRValueFromEUR(eurText: 1.00, targetValue: convertModel?.rates?.iNR ?? 0.0)
        XCTAssertNotEqual(value,33.40)
    }
    
    override func tearDown() {
        super.tearDown()
        converterViewModel = nil
        convertModel = nil
    }
}

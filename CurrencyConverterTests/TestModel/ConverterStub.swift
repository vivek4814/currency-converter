/**
* Copyright (c) 2021 HCL, Inc.. All rights reserved.
*/

import Foundation
@testable import CurrencyConverter

class ConverterStub {
    
    static func converter(file name: String) -> Data? {
      if let path = Bundle(for: self).path(forResource: name, ofType: "json") {
        do {
          return try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
        } catch let error {
          print("parse error: \(error.localizedDescription)")
        }
      } else {
        print("Invalid filename/path.")
      }
      return nil
    }
}

/**
 * Copyright (c) 2021 HCL, Inc.. All rights reserved.
 */

import Foundation
import UIKit

var APP_DELEDATE  = UIApplication.shared.delegate as! AppDelegate

enum Constants {
    
    static let base_url = "http://data.fixer.io/api/latest?access_key=65ecee4e7e3b0ba98bb83922e501c98a"
    static let internet_connection_message = "Please check your internet connection"
    static let internet_message_comment = "internet connection message"
    static let indian_rupee = "Indian Rupee"
    static let eur_equal = "EUR equals"
    static let eur_default_value = "EUR default value"
    static let inr_value = "INR value"
    static let eur_calculated_value = "EUR calculated value"
    static let inr_calculated_value = "INR calculated value"
    static let eur_intial_value   = "EUR intial value"
    static let inr_intial_value   = "INR intial value"
}

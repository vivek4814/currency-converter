/**
 * Copyright (c) 2021 HCL, Inc.. All rights reserved.
 */

import UIKit

extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

//MARK: Extension UIViewController
extension UIViewController {
    

    
    func showOkAlert(_ msg: String) {
        let alert = UIAlertController(title:
                                        "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    
}
public extension NSDictionary {
    func toData(jsonPath : String = "") -> Data{
        let dataDict = self.value(forKeyPath: jsonPath)
        do{
            let dataJson = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return dataJson
        }catch let error{}
        return Data()
    }
    
}

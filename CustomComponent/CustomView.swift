/**
* Copyright (c) 2021 HCL, Inc.. All rights reserved.
*/

import UIKit

class CustomView: UIView {

    @IBInspectable
    var borderWidth: CGFloat {
      get {
        return layer.borderWidth
      }
      set {
        layer.borderWidth = newValue
      }
    }

   
    @IBInspectable
     var borderColor: UIColor? {
       get {
         if let color = layer.borderColor {
           return UIColor(cgColor: color)
         }
         return nil
       }
       set {
         if let color = newValue {
           layer.borderColor = color.cgColor
         } else {
           layer.borderColor = nil
         }
       }
     }

}

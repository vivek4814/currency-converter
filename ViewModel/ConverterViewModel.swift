/**
 * Copyright (c) 2021 HCL, Inc.. All rights reserved.
 */

import UIKit

class ConverterViewModel: NSObject {
    
    private var apiService : BaseServiceImpl!
    
    private var converterData : [Converter]? {
        didSet {
            self.bindConverterViewModelToView()
        }
    }
    var bindConverterViewModelToView : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  BaseServiceImpl.sharedInstance
        self.converterData = [Converter]()
        response()
    }
    
    private func response() {
        self.getDataFromServer()
    }
    
    func returnRatesObj() -> Rates? {
        return self.converterData?[0].rates
    }
    
    // Get Converted EUR value from  INR
    func getConvertedINRValueFromEUR(eurText : Double, targetValue : Double) -> Double {
        var double = 0.0
        double = eurText * targetValue
        return double
    }
    
    // Get Converted INR value from  EUR
    func getConvertedEURValueFromINR(inrText : Double, targetValue : Double) -> Double {
        var double = 0.0
        double = inrText / targetValue
        return double
    }
    
    // Get Data From Srever
    private func getDataFromServer() -> Void {
        SwiftLoader.show(title: "Please wait", animated: true)
        self.apiService.requestApiWithUrlString(requestType: HtttpType.GET.rawValue, baseUrl: Constants.base_url) { (dataObj, _error, success) in
            if success {
                var dataArray = [Converter]()
                if let response = dataObj {
                    if let converterModel = Converter.objData(fromDict: response as NSDictionary){
                        dataArray.append(converterModel)
                        self.converterData = dataArray
                    }
                }
                
            }
            DispatchQueue.main.async {
                SwiftLoader.hide()
            }
        }
    }
}

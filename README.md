# README #

This project is a simple currency converter app for iOS. Cureently it's doing currency converter between two currency EUR and INR.

## Motivation

This project is a learning exercise in Swift programming. I seek primarily to get feedback and critique on the code written for this project, but feedback on the overall design and idea is also


# Requirements

* iOS 13.0+ 
* Xcode 11.4

## Swift

This project is written in Swift 5.

## API Reference

Get the latest foreign exchange reference rates in JSON format.

```http
GET /latest
Host: api.fixer.io
```

# Unit testing
ViewModel and models are covered with unit tests.
Check "TestModel folder && TestViewModel".

See files:  
ConverterViewModelTests.m   

for testing XCode -> Product -> Test . 

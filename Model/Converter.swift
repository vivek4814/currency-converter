/**
* Copyright (c) 2021 HCL, Inc.. All rights reserved.
*/

import UIKit

struct Converter: Codable {
    let success : Bool?
    let timestamp : Int?
    let base : String?
    let date : String?
    let rates : Rates?
    let error : JsonError?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case timestamp = "timestamp"
        case base = "base"
        case date = "date"
        case rates = "rates"
        case error = "error"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        timestamp = try values.decodeIfPresent(Int.self, forKey: .timestamp)
        base = try values.decodeIfPresent(String.self, forKey: .base)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        rates = try values.decodeIfPresent(Rates.self, forKey: .rates)
        error = try values.decodeIfPresent(JsonError.self, forKey: .error)

    }
    
    static func objData(fromDict : NSDictionary)-> Converter?{
        do {
            let decoder = JSONDecoder()
            let data = fromDict.toData()
            return try decoder.decode(Converter.self, from:data)
        } catch _ {
            
        }
        return nil
    }
}

struct JsonError : Codable {
    let code : Int?
    let type : String?
    let info : String?

}

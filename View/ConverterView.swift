/**
* Copyright (c) 2021 HCL, Inc.. All rights reserved.
*/

import UIKit

class ConverterView: UIViewController {
    
    @IBOutlet weak var eurDesLbl: UILabel!
     @IBOutlet weak var inrTxt: UITextField!
     @IBOutlet weak var eurTxt: UITextField!
     @IBOutlet weak var inrDesLbl: UILabel!

     private var converterViewModel  = ConverterViewModel()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureView()
            addDoneButtonOnKeyboard()
        }
        
        // configure view and check internet connection
        fileprivate  func configureView() -> Void {
            eurTxt.delegate = self
            inrTxt.delegate = self
            if Reachability.isConnectedToNetwork() == true {
                callToViewModelForUIUpdate()
            }else {
                self.showOkAlert(NSLocalizedString(Constants.internet_connection_message, comment: Constants.internet_message_comment))
            }
        }
        
        //intialize view model and give intial value
        fileprivate func callToViewModelForUIUpdate(){
            self.converterViewModel.bindConverterViewModelToView = {
                    DispatchQueue.main.async {
                        self.setupIntialValue()
                    }
              }
            }
        }


    extension ConverterView {
        
        fileprivate func setupIntialValue() -> Void {
            
            let y = Double(round(1000*(self.converterViewModel.returnRatesObj()?.iNR ?? 0.0))/1000)
            eurTxt.text = NSLocalizedString("1", comment: Constants.eur_default_value)
            inrTxt.text = NSLocalizedString("\(y)", comment: Constants.inr_value)
            eurDesLbl.text = NSLocalizedString("1 \(Constants.eur_equal)", comment: Constants.eur_equal)
            inrDesLbl.text =  NSLocalizedString("\(y) \(Constants.indian_rupee)", comment: Constants.indian_rupee)
        }
        
        //Add done button on keyboard
        fileprivate func addDoneButtonOnKeyboard(){
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            doneToolbar.barStyle = .default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
            
            let items = [flexSpace, done]
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            
            inrTxt.inputAccessoryView = doneToolbar
            eurTxt.inputAccessoryView = doneToolbar
        }
        
        @objc func doneButtonAction(){
            self.view.endEditing(true)
        }
    }

    // UItextfiled delegate
    extension ConverterView : UITextFieldDelegate {
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            // get input character
            let previousText:NSString = textField.text! as NSString
            let updatedText = previousText.replacingCharacters(in: range, with: string)
            // if user select INR input textfield
            if textField == inrTxt {
                if updatedText.count > 0 {
                        let value = self.converterViewModel.getConvertedEURValueFromINR(inrText: ((updatedText as NSString).doubleValue), targetValue: (self.converterViewModel.returnRatesObj()?.iNR ?? 0.0))
                        
                        //get round value upto three value after decimal
                        let y = Double(round(1000*value)/1000)
                        eurTxt.text =  NSLocalizedString("\(y)", comment: Constants.eur_calculated_value)
                  
                } else {
                    // Set value when input text is empty
                    eurTxt.text =  NSLocalizedString("0", comment: Constants.eur_intial_value)
                    inrTxt.text = NSLocalizedString("0", comment: Constants.inr_intial_value)
                }
            }
            
            // if user select EUR input textfield
            if textField == eurTxt {
                if updatedText.count > 0 {
                        
                        let value = self.converterViewModel.getConvertedINRValueFromEUR(eurText: ((updatedText as NSString).doubleValue), targetValue: (self.converterViewModel.returnRatesObj()?.iNR ?? 0.0))
                        
                        //get round value upto three value after decimal
                        let y = Double(round(1000*value)/1000)
                        inrTxt.text =  NSLocalizedString("\(y)", comment: Constants.inr_calculated_value)
                   
                } else {
                    
                    // Set value when input text is empty
                    eurTxt.text =  NSLocalizedString("0", comment: Constants.eur_intial_value)
                    inrTxt.text = NSLocalizedString("0", comment: Constants.inr_intial_value)
                }
            }
            return true
        }
    }

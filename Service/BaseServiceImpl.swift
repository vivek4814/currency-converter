/**
 * Copyright (c) 2021 HCL, Inc.. All rights reserved.
 */

import UIKit

struct RequestType {
    static let  POST = "POST"
    static let  GET = "GET"
}

enum HtttpType: String {
    case POST = "POST"
    case GET  = "GET"
}

enum ErrorType {
    case invalidUrl
    case serverError
    case invalidJson
    case dataNotFound

}

class BaseServiceImpl: NSObject {
    
  

    //Define singlton
    static let sharedInstance: BaseServiceImpl = {
        let instance = BaseServiceImpl()
        return instance
    }()
    
    private override init() {}
    
    //Define method for rquest with url
    public func requestApiWithUrlString(requestType: String, baseUrl : String, completionHendler: @escaping(_ response: [String : Any]?, _ error: ErrorType?, _ success: Bool) -> Void ) {
        guard let url = URL(string: baseUrl) else {
            debugPrint("Error: cannot create URL")
            completionHendler(nil, ErrorType.invalidUrl, false)
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                debugPrint("error calling GET on /todos/1")
                completionHendler(nil, ErrorType.serverError, false)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                debugPrint("Error: did not receive data")
                completionHendler(nil, ErrorType.dataNotFound, false)
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String:Any] else {
                        debugPrint("error trying to convert data to JSON")
                        return
                }
                debugPrint("todo:-\(todo)")
                completionHendler(todo, nil, true)
            } catch  {
                print("error trying to convert data to JSON")
                completionHendler(nil, ErrorType.invalidJson, false)

                return
            }
        }
        task.resume()
    }
}
